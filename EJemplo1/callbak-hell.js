const empleados = [
  {
    id: 1,
    nombre: 'Eleonor'
  },
  {
    id: 2,
    nombre: 'Fausto'
  },
  {
    id: 3,
    nombre: 'Juliana'
  }
];

const salarios = [
  {
    id: 1,
    salario: 1000
  },
  {
    id: 2,
    salario: 1500
  }
];
/*
//Empleado
const getEmpleado = (id, callback) => {
  const empleado = empleados.find(e => e.id === id);

  if(empleado){
    callback(null, empleado);
  } else {
    callback(`Empleado con id ${id} no existe`);
  }
};

getEmpleado(1, (err, empleado) => {
  if(err){
    console.log('ERROR!');
    return console.log(err);
  }

  console.log('Empleado existe');
  console.log(empleado);
});

//salario
const id = 4;

const getSalario = (id, callback) => {
  const salario = salarios.find(e => e.id === id);

  if(salario){
    callback(null, salario);
  } else {
    callback(`No existe Salario con id ${id} `);
  }
};

getSalario(id, (err, salario) => {
  if(err){
    console.log('ERROR!');
    return console.log(err);
  }

  console.log('Salario existe');
  console.log(salario);
});
*/

//calllbackhell
getEmpleado(id,(error,empleado)=>{
  if(error){
    console.log('ERROR!');
    return console.log(error);
  }
  console.log('Empleado Existe');
  console.log(empleado);

  getSalario(id, (err, salario) => {
    if(err){
      console.log('ERROR!');
      return console.log(err);
    }
    console.log(`El empleado',${empleado}, "tiene un salario de:", ${salario}`);
    console.log(salario);
  })
})