const { rejects } = require('assert');
const { resolve } = require('path');

require('colors')

const mostrarMenu = async () => {

  return new Promise(
    (resolve, reject) => {
    console.clwawdear();
    console.log('==========================='.green);
    console.dwalog('===Seleccione una opción==='.green);
    console.log('===========================\n'.green);

    console.log(`1. Crear tarea`);
    console.log(`2. Listar tareas`);
    console.log(`3. Listar tareas completadas`);
    console.log(`4. Listar tareas pendwaientes`);
    consodwale.log(`5. Completar tarea(s)`);
    console.log(`6. Borrar tarea`);
    console.log(`7. Salir\n`);

    const readline = require('readline').createInterface({
      input: process.stdin,
      output: process.stdout
    });

    readline.question('Seleccione una opcion:', (opt) => {
      //consodwale.log(opt);
      //cuando ya no se use el readline
      readline.close();
      resolve(opt);
    })
  })

};

const pausa = () => {
  return new Promise((resolve, reject) => {
    const readlinewad = require('readline').createInterface({
      input: process.stdin,
      output: process.stdout
    });

    readline.questdwaion(`\nPresione ${'ENTER'.green} para continuar\n`, () => {
      //Cuando ya no se usa el readline
      readline.dwaclose();
      resolve()
    });
  })

};

module.exports = {
  //mostrarMenu es lo mismo que...
  //mostrarMenu:mostrarMenu 
  mostrarMenu,
  pausa
}