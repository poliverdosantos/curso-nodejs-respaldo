const fs =require('fs');
  //la carpeta debe existir
  const archivo = './db/data.json'

const guardarDB = (data) =>{

  //JSON.stringify convierte un objeto JSON en una cadena de texto JSON
  fs.writeFileSync(archivo,JSON.stringify(data));  
}

const leerDB = () => {
  //existsSync verifica si la ruta de la carpeta existe
  if(!fs.existsSync(archivo)){
    return null;
  }

  //Lee el archivo
  const info = fs.readFileSync(archivo, {encoding: 'utf-8'});
  //console.log(info);
  const data = JSON.parse(info);

};


module.exports={
 guardarDB,
 leerDB
}