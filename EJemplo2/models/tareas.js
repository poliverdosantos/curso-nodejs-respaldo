const Tarea = require("./tarea");
_listado={}

class Tareas {

  constructor() {
    this._listado = {};
  }

  crearTarea(desc = ''){
    const tarea = new Tarea(desc);
    this._listado[tarea.id] = tarea;
  }

  get listadoArr() {
    const listado = [];
    //Me devuelve un arreglo de todas las keys
    Object.keys(this._listado).forEach(key => {
      // console.log(key);
      const tarea = this._listado[key];
      listado.push(tarea);

    });

    return listado;
  }

  cargarTareaformArray(tareas = []){
    tareas.forEach(tarea =>{
      this._listado[tarea.id] = tarea;
    })
  }
}

module.exports=Tareas