//const { mostrarMenu, pausa } = require('./helpers/mansajes');
const {
  inquirerMenu,
  pausa,
  leerInput
} = require(`./helpers/inquirer`);

const Tareas = require('./models/tareas');
const Tarea = require('./models/tarea');
const { guardarDB, leerDB } = require('./helpers/guardarArchivo');
require('colors');

//Para limpiar la consola
//console.clear();

const main = async () => {
  console.log('Hola Mundo');


  let opt = '';
  const tareas = new Tareas();
  const tareasDB =  leerDB();


  do {
    opt = await inquirerMenu();
    //console.log({opt});

    switch (opt) {
      case '1':
        //Crear opcion
        const desc = await leerInput('Descripcion:');
        //console.log(desc);
        tareas.crearTarea(desc);
        break;
      case '2':
        console.log(tareas.listadoArr)
        break;
    }
    guardarDB(tareas.listadoArr)

  if(tareasDB){
    //Establecer las tareas
    tareas.cargarTareaformArray(tareasDB)
  } 
  await pausa();

  } while (opt !== '7');
};

main();