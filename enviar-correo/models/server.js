const express = require('express');

class Server {
  constructor(){
    this.app = express()
    this.port = process.env.PORT;
    this.emailPath = '/api/enviar-email'

     //Middlewares
     this.middlewares();
     //Rutas de mi aplicacion
     this.routes();
 
  }

  middlewares(){
    //lectura y parseo del body
    //cualquier informacion que venga se serializa en formato JSON
    this.app.use(express.static('public'));
  }

  routes(){
    this.app.use(this.emailPath,require('../routes/email'))
  }

  listen(){
    // COn procee.env.PORT obtenemos el valor de la variable PORT que esta en el archivo .env 
    this.app.listen(this.port,()=>{
      console.log('Servidor corriendo en el puerto', this.port);
    });
  }
}
