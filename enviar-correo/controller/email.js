const { response, request } = 'express';
const { transporter } = require('../config/mailer');

const emailPost = async (req = request, res = response) => {

  // send mail with defined transport object
  await transporter.sendMail({
    from: `Mensaje de Formulario de Contacto - Coca Cola ${process.env.EMAIL}`, // sender address
    to: process.env.EMAIL, // list of receivers
    subject: "Formulario de Contacto", // Subject line
    text: "Contenido del mensaje", // plain text body
    //html: "<b>Hello world?</b>", // html body
  });
  
  res.status(200).json({
    message: 'OK'
  });
}

module.exports = { emailPost };