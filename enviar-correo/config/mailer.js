const nodemailer = require("nodemailer");

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    user: process.env.EMAIL, // generated ethereal user
    pass: process.env.PASSWORD_EMAIL, // generated ethereal password
  },
});

transporter.verify().then(() => {
  console.log('Listo para enviar emails');
});

module.exports = { transporter };