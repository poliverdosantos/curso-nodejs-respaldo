const http = require('http');
const { type } = require('os');
/*
Es un callback que tiene los 2 parametros:
request = Es lo que se esta solicitando, toda la informacion de la url que se solicita,
los headers de la peticion, argumentos
response = Es lo que el servidor le va a responder al cliente
*/
http.createServer((request, response) => {

  console.log(request);

  //response.writeHead(200,{'Content-Type': 'application/json'})
  response.setHeader('Content-Disposition', 'attachment; filename=lista.csv');
  response.writeHead(200, { 'Content-Type': 'application/csv' });

  //Escriir una respuesta 
  response.write('id, nombre\n')
  response.write('2,Mercedes\n')
  response.write('3,Juan\n')
  response.write('4,Pedro\n')


  //Terrminar Respuesta 
  response.end();
})
  .listen(8080);
console.log('Escuchando el puerto', 8080)