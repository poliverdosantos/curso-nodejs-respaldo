
const express = require('express')
const res = require('express/lib/response');
const app = express()
const port = 8080;

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.send('Home Page')
})

app.get('/elements', (req, res) => {
  res.sendFile(__dirname + '/public/elements.html');
});

app.get('/generic', (req, res) => {
  res.sendFile(__dirname + '/public/generic.html');
});


app.get('*', (req, res) => {
  res.send('404 | Page not found')
});

app.listen(port, () => {
  console.log(`app escuchando en http://localhost:${port}`);
});


app.listen(8080)